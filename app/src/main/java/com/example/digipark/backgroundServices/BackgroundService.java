package com.example.digipark.backgroundServices;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.digipark.MainActivity;
import com.example.digipark.R;


@RequiresApi(api = Build.VERSION_CODES.M)
public class BackgroundService extends Service {

    private static final int NOTIF_ID = 666;    // Unique ID for each notification that must be defined
    private static final String NOTIF_CHANNEL_ID = "DigiParkNotifCh";
    public static boolean parkingMeterStarted = false;
    public static int[] parkedSince = {0,0};
    NotificationManager notificationManager = null;

    // Showing ongoing notification that the service is running
    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        startForeground(NOTIF_ID, new NotificationCompat.Builder(this,
                NOTIF_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_home_logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.bs_notif_content))
                .setContentIntent(pendingIntent)
                .build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        parkingMeterStarted = false;
        if (notificationManager!=null)
            notificationManager.cancel(NOTIF_ID);   // Cancel the notification
    }
}
