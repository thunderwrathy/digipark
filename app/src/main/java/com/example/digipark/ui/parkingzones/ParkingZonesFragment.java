package com.example.digipark.ui.parkingzones;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.digipark.R;

import com.example.digipark.jsonreader.JsonReader;
import com.example.digipark.sharedprefhelper.SharedPreferencesHelper;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.geojson.MultiPolygon;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.OnLocationClickListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.turf.TurfJoins;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

import static com.example.digipark.sharedprefhelper.SharedPreferencesHelper.getSharedPreferencesString;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;


public class ParkingZonesFragment extends Fragment implements LocationListener, OnLocationClickListener {

    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private MapView mapView;
    MapboxMap mbMap;
    Style mapStyle;
    LocationComponent locationComponent;
    boolean locCompPaused = true;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_parking_zones, container, false);

        // Location permission check
        locationManager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);

        String[] permissions = {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        } else {
            ActivityCompat.requestPermissions(getActivity(), permissions, 101);
        }

        // Create asynchronous map
        final JsonReader jr = new JsonReader();
        final String[] jsonString = {null};
        final String[] mapBoxStyle = new String[1];

        String lat = getSharedPreferencesString(getContext(), "StpzCityLat", "47.79");
        String lon = getSharedPreferencesString(getContext(), "StpzCityLon", "13.04");

        double latitude = Double.parseDouble(lat);         // Displays Salzburg as default
        double longitude = Double.parseDouble(lon);

        MapboxMapOptions options = MapboxMapOptions.createFromAttributes(getContext(), null)
                .camera(new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude))
                        .zoom(11)
                        .build());

        mapView = new MapView(getContext(), options);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mbMap = mapboxMap;

                if (getSharedPreferencesString(getContext(), "mapColor", getString(R.string.settings_map_dark)).equals(getString(R.string.settings_map_light))) {
                    mapBoxStyle[0] = Style.TRAFFIC_DAY;
                } else {
                    mapBoxStyle[0] = Style.TRAFFIC_NIGHT;
                }

                mapboxMap.setStyle(mapBoxStyle[0], new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        mapStyle = style;
                        locCompPaused = false;
                        String geometryStr;
                        JSONArray geometryArr;

                        try {
                            InputStream fileStream = null;
                            switch(getSharedPreferencesString(getContext(), "StpzCity", "sbg")) {
                                case "sbg":
                                    fileStream = getResources().openRawResource(R.raw.st_parking_zones_sbg);
                                    break;
                                case "vienna":
                                    fileStream = getResources().openRawResource(R.raw.st_parking_zones_vienna);
                                    break;
                            }

//                            jsonString = readStringFromURL("https://www.data.gv.at/katalog/dataset/stadt-wien_kurzparkzonenwien/resource/d2d24f79-0dde-46a2-be9e-f3c34417cf20");
                            jsonString[0] = jr.stringFromJsonFilestream(fileStream);
                            geometryArr = jr.getGeometryArray(jsonString[0]);

                            Random rnd = new Random(420247);
                            for (int i = 0; i<geometryArr.length(); i++) {
                                geometryStr = geometryArr.get(i).toString();
                                GeoJsonSource geoSrc = new GeoJsonSource("polygon"+i, geometryStr);
                                style.addSource(geoSrc);

                                FillLayer singlePolyFill = new FillLayer("single-poly-fill"+i, "polygon"+i);
                                int randomColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                singlePolyFill.setProperties(
                                        fillColor(randomColor),
                                        fillOpacity(0.4f)
                                );
                                style.addLayerBelow(singlePolyFill, "water");
                            }
                        }
                        catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                mapboxMap.addOnMapClickListener(new MapboxMap.OnMapClickListener() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public boolean onMapClick(@NonNull LatLng point) {

                        // Determining which short-time parking zone device is in and getting showing information dialog
                        String geometryStr;
                        JSONArray geometryArr;
                        Polygon polygon = null;
                        MultiPolygon multiPolygon = null;

                        Point point1 = Point.fromLngLat(point.getLongitude(), point.getLatitude());
                        InputStream fileStream = null;
                        switch(getSharedPreferencesString(getContext(), "StpzCity", "sbg")) {
                            case "sbg":
                                fileStream = getResources().openRawResource(R.raw.st_parking_zones_sbg);
                                break;
                            case "vienna":
                                fileStream = getResources().openRawResource(R.raw.st_parking_zones_vienna);
                                break;
                        }

                        int i = 0;
                        try {
                            jsonString[0] = jr.stringFromJsonFilestream(fileStream);
                            geometryArr = jr.getGeometryArray(jsonString[0]);
                            Log.e("GEO", geometryArr.toString());
                            boolean locInsidePolygon = false;
                            boolean locInsideMultiPolygon = false;

                            while (i < geometryArr.length() && !locInsidePolygon && !locInsideMultiPolygon) {
                                geometryStr = geometryArr.get(i).toString();
                                if (geometryStr.contains("MultiPolygon")) {
                                    multiPolygon = MultiPolygon.fromJson(geometryStr);
                                    locInsideMultiPolygon = TurfJoins.inside(point1, multiPolygon);    // Checking if GPS location is inside MultiPolygon
                                }
                                else {
                                    polygon = Polygon.fromJson(geometryStr);
                                    locInsidePolygon = TurfJoins.inside(point1, polygon);    // Checking if GPS location is inside polygon
                                }
                                i++;
                            }
                            if (i < geometryArr.length()) {
                                String[] properties;
                                JSONObject propertiesObj = jr.getStpzProperties(jsonString[0], i);

                                String stpzCity = SharedPreferencesHelper.getSharedPreferencesString(getContext(), "StpzCity", "sbg");
                                properties = jr.getPropertiesInfoArr(propertiesObj, stpzCity);
                                DialogFragment pzi = new ParkingZoneInfo(properties, stpzCity);
                                pzi.show(getFragmentManager(), "Info");
                            }
                        }
                        catch (JSONException | IOException e) {
                            Log.e("Showing PZI failed", Arrays.toString(e.getStackTrace()));
                        }
                        return true;
                    }
                });
            }
        });

        FrameLayout frameMapView = root.findViewById(R.id.frameMapView);
        frameMapView.addView(mapView);

        return root;
    }

    public static String readStringFromURL(String requestURL) throws IOException
    {
        try (Scanner scanner = new Scanner(new URL(requestURL).openStream(),
                StandardCharsets.UTF_8.toString()))
        {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    // LocationListener

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(@NonNull Location location) {
        if (!locCompPaused) {
            try {
                locationComponent = mbMap.getLocationComponent();
                locationComponent.activateLocationComponent(
                        LocationComponentActivationOptions
                                .builder(getContext(), mapStyle)
                                .useDefaultLocationEngine(true)
                                .locationEngineRequest(new LocationEngineRequest.Builder(750)
                                        .setFastestInterval(750)
                                        .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                                        .build())
                                .build());
                locationComponent.setLocationComponentEnabled(true);
            }
            catch (NullPointerException e) {
                Log.e("Error LocComponent", e.toString());
                locCompPaused = true;
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.e("GPS",provider + " disabled");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.e("GPS",provider + " enabled");
    }

    @Override
    public void onLocationComponentClick() {
    }
}

