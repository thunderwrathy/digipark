package com.example.digipark.ui.home;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.digipark.R;
import com.example.digipark.backgroundServices.BackgroundService;
import com.example.digipark.jsonreader.JsonReader;
import com.example.digipark.mqtthelper.MqttHelper;
import com.example.digipark.sharedprefhelper.SharedPreferencesHelper;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.turf.TurfJoins;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class HomeFragment2 extends Fragment implements LocationListener {

    Runnable updater;
    MqttHelper mqtthelper;
    LocationManager locationManager;
    NotificationManager notificationManager = null;
    String clientId = null;
    int notificationId = 420;
    int[] parkedSince;
    final int[] finalAllowedParkDuration = {25, 0};
    final int MODE_PARK_ZONE_DETECT = 25;

    static String comTopic = "digipark/com";
    static String tcnTopic = "digipark/data/tcn";
    static String micTopic = "digipark/data/max4466";


    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home2, container, false);
        final TextView tv_mqtt = root.findViewById(R.id.tv_mqtt);
        final TextView tv_mic = root.findViewById(R.id.tv_dB);
        final TextView tv_C = root.findViewById(R.id.tv_C);
        final String CHANNEL_ID = getString(R.string.dp_channel1);

        // Setting callback and connecting to HiveMQ MQTT broker
        mqtthelper = new MqttHelper(getContext(), "tcp://broker.hivemq.com:1883", "testClient"+clientId, "user", "pass");
        mqtthelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                tv_mqtt.setText(R.string.tv_mqtt_connect_complete);
                mqtthelper.publishToTopic(comTopic, "start");     // Send command "start" to ESP over MQTT to start parking meter
                mqtthelper.subscribeToTopic(micTopic);
                mqtthelper.subscribeToTopic(tcnTopic);
            }

            @Override
            public void connectionLost(Throwable throwable) {
                tv_mqtt.setText(R.string.tv_mqtt_connection_lost);
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                tv_mqtt.setText(getString(R.string.tv_mqtt_msg_arrived) + mqttMessage);
                int data = Integer.parseInt(mqttMessage.toString());
                if (topic.equals(tcnTopic)) {
                    tv_C.setText(mqttMessage.toString()+" °C");

                    if ((data > SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "TempThresh", 40)) &&
                            SharedPreferencesHelper.getSharedPreferencesBoolean(getContext(), "TempThreshEnable", false))  {
                        showTempAlert(data , CHANNEL_ID);
                    }
                }
                if (topic.equals(micTopic)) {
                    double percent = (data/4095.0) * 100;
                    tv_mic.setText(""+(int)percent+"%");
                    int micThreshold = SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "MicThresh", 30);
                    if ((percent > (100-micThreshold)) && SharedPreferencesHelper.getSharedPreferencesBoolean(getContext(), "MicThreshEnable", false)) {
                        showMicAlert(micThreshold, CHANNEL_ID);
                    }
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                tv_mqtt.setText(R.string.tv_mqtt_delivery_completed);
            }
        });
        mqtthelper.connect();

        // Giving the stop button functionality
        root.findViewById(R.id.button_stop).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                mqtthelper.publishToTopic(comTopic, "stop");
                mqtthelper.closeConnection();
                BackgroundService.parkingMeterStarted = false;
                requireActivity().stopService(new Intent(getActivity(), BackgroundService.class));      // Stops the background service
                NavController navController = NavHostFragment.findNavController(HomeFragment2.this);
                navController.navigate(R.id.action_homeFragment2_to_nav_home);
            }
        });

        // If parking meter was already running before entering view, set previously determined max allowed parking duration
        if (BackgroundService.parkingMeterStarted) {
            finalAllowedParkDuration[0] = SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "AllowedParkDuration0", 25);
        }

        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public void onViewCreated(@NonNull final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        locationManager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);

        // Checking permission for fine and coarse location
        String[] permissions = {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        } else {
            ActivityCompat.requestPermissions(requireActivity(), permissions, 101);
        }

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

//        // Checking permission for microphone and setting up MediaRecorder
//        if (ActivityCompat.checkSelfPermission(getContext(), RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(requireActivity(), new String[]{RECORD_AUDIO},
//                    10);
//        }
//        mRecorder = new MediaRecorder();
//        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//        mRecorder.setOutputFile("/dev/null");
//        try {
//            mRecorder.prepare();
//        } catch (IllegalStateException | IOException e) {
//            e.printStackTrace();
//        }
//        mRecorder.start();

        // Setting the start parking time (parkedSince) to current time if the parking meter was not started yet
        if (!BackgroundService.parkingMeterStarted) {
            Log.e("START", "Sent start command");
            mqtthelper.publishToTopic(comTopic, "start");
            BackgroundService.parkedSince = getCurrentTime();
        }
        parkedSince = BackgroundService.parkedSince;

        // Showing time that is left before exceeding maximum parking duration
        final TextView tv_minute = view.findViewById(R.id.tv_minutes);
        final TextView tv_hour = view.findViewById(R.id.tv_hours);
        final List<Integer> alertTimes = new ArrayList<>();

        for(int i = 0; i < SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "NotifCount", 0); i++) {  // Getting the alert times that were
            alertTimes.add(i, SharedPreferencesHelper.getSharedPreferencesInt(getContext(), ("Notif"+i), 0));           // set in the settings and stored in
        }                                                                                                                       // the SharedPreferencesHelper

        final String CHANNEL_ID = getString(R.string.dp_channel1);
        createNotificationChannel(CHANNEL_ID);

        final LinearLayout lin_lay_remaining_time = view.findViewById(R.id.lin_lay_remaining_time);
        final ImageButton ib_warning = view.findViewById(R.id.ib_warning_sign);
        ib_warning.setVisibility(View.GONE);
        final TextView tv_remaining_time = view.findViewById(R.id.tv_info_string);
        final boolean[] warning_sign_toggled = {true};
        final boolean[] first_loop = {true};
        final TextView tv_db = view.findViewById(R.id.tv_dB);
        


        final Handler timerHandler = new Handler();
        updater = new Thread(new Runnable() {
            @Override
            public void run() {

                if (finalAllowedParkDuration[0] == MODE_PARK_ZONE_DETECT) {     // In detection mode: Getting max allowed parking duration
                    Log.i("MODE", "In detection mode");
                }
                else {                                                        // In operation mode: Calculating of remaining time & Showing notifications
                    int[] time_now = getCurrentTime();
                    int min_left_ges = (parkedSince[0] * 60 + parkedSince[1]) +
                            (finalAllowedParkDuration[0] * 60 + finalAllowedParkDuration[1]) -
                            (time_now[0] * 60 + time_now[1]);
                    int hours_left = min_left_ges / 60;
                    int min_left = min_left_ges - hours_left * 60;

                    /* Still time left */
                    if (min_left_ges > 0) {
                        tv_hour.setText(hours_left + "");
                        tv_minute.setText(min_left + "");
                        if (min_left < 10)
                            tv_minute.setText("0" + Integer.toString(min_left));
                        if (hours_left < 10)
                            tv_hour.setText("0" + hours_left);
                    }

                    /* When parking duration has been exceeded */
                    else {
                        if (first_loop[0])
                            lin_lay_remaining_time.setVisibility(View.GONE);
                        tv_remaining_time.setTextSize(25);
                        tv_remaining_time.setGravity(Gravity.CENTER);
                        first_loop[0] = false;

                        if (hours_left == 0)
                            tv_remaining_time.setText(getString(R.string.tv_time_exceeded) + " " + (-min_left) + "min" + "!");
                        else
                            tv_remaining_time.setText(getString(R.string.tv_time_exceeded) + " " + (-hours_left) + "h " + (-min_left) + "min" + "!");

                        if (warning_sign_toggled[0]) {
                            ib_warning.setBackgroundResource(R.drawable.warning_sign01);
                            tv_remaining_time.setTextColor(ContextCompat.getColor(
                                    requireContext(), R.color.text_toggled));
                        } else {
                            ib_warning.setBackgroundResource(R.drawable.warning_sign02);
                            tv_remaining_time.setTextColor(ContextCompat.getColor(
                                    requireContext(), R.color.text_normal));
                        }
                        warning_sign_toggled[0] = !warning_sign_toggled[0];
                        ib_warning.setVisibility(View.VISIBLE);
                    }
                    for (int i = 0; i < alertTimes.size(); i++) {
                        if (min_left_ges == alertTimes.get(i)) {
                            alertTimes.remove(i);
                            showAlert(min_left_ges, CHANNEL_ID);
                        }
                    }

//                    tv_db.setText(getCurrentDbfs() + " " + getString(R.string.tv_dB));
                }
                timerHandler.postDelayed(this, 1000);
            }
        });
        timerHandler.post(updater);
    }

    public int[] getAllowedParkingDuration(Location loc) {

        // Checking permission for fine and coarse location
        String[] permissions = {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        } else {
            ActivityCompat.requestPermissions(requireActivity(), permissions, 101);
        }

        // Determining which short-time parking zone device is in and getting maximum parking duration
        String jsonString;
        String geometryStr;
        JSONArray geometryArr;
        Polygon polygon;
        JsonReader jr = new JsonReader();
        int[] allowedParkingDuration;
        Point point = Point.fromLngLat(loc.getLongitude(), loc.getLatitude());
        InputStream fileStream = getResources().openRawResource(R.raw.st_parking_zones_sbg);

        try {
            jsonString = jr.stringFromJsonFilestream(fileStream);
            geometryArr = jr.getGeometryArray(jsonString);

            boolean locInsidePolygon = false;
            int i = 0;
            while (i < geometryArr.length() && !locInsidePolygon) {
                geometryStr = geometryArr.get(i).toString();
                polygon = Polygon.fromJson(geometryStr);
                locInsidePolygon = TurfJoins.inside(point, polygon);    // Checking if GPS location is inside polygon
                i++;
            }
            i--;

            JSONObject propertiesObj = jr.getStpzProperties(jsonString, i);
            allowedParkingDuration = jr.getAllowedParkTime(propertiesObj);
            return allowedParkingDuration;
        }
        catch (Exception e) {
            Log.e("ERROR", "Error while determining which short-term parking zone device is in. \n" + Arrays.toString(e.getStackTrace()));
        }
        return new int[]{25, 0};
    }

    public int[] getCurrentTime(){
        Calendar c = Calendar.getInstance();
        return new int[]{c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE)};
    }

//    public int getCurrentDbfs(){
//        return (int) (20 * Math.log10(mRecorder.getMaxAmplitude() / 32767.0));
//    }

    private void createNotificationChannel(String CHANNEL_ID) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {   // API 26+ only
            CharSequence name = getString(R.string.notification_channel_name);
            String description = getString(R.string.notification_channel_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Registering the channel with the system; Can't change the importance or other notification behaviors after this
            notificationManager = (NotificationManager) getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void showAlert(int min_left, String CHANNEL_ID) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_home_logo)
                .setContentTitle(getString(R.string.alert_msg_1) +" "+ min_left +" "+ getString(R.string.alert_msg_2))
                .setContentText(getString(R.string.notification_text))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        notificationManager.notify(notificationId, builder.build());
    }

    public void showTempAlert(int temp, String CHANNEL_ID) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_home_logo)
                .setContentTitle(getString(R.string.alert_temp1) + " "+temp +"°C")
                .setContentText(getString(R.string.notification_text))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        notificationManager.notify(notificationId, builder.build());
    }

    public void showMicAlert(int data, String CHANNEL_ID) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_home_logo)
                .setContentTitle(getString(R.string.alert_mic1) +" "+ (100-SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "MicThresh", 30)) +" "+ getString(R.string.alert_mic2))
                .setContentText(getString(R.string.notification_text))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        notificationManager.notify(notificationId, builder.build());
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getString(R.string.dialog_gps_en_info))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.dialog_gps_positive), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getString(R.string.dialog_gps_negative), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onLocationChanged(@NonNull Location location) {

        if (finalAllowedParkDuration[0] == MODE_PARK_ZONE_DETECT){
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                int[] allowedParkDuration;
                if (!BackgroundService.parkingMeterStarted) {
                    mqtthelper.publishToTopic(comTopic, "start");
                }
                BackgroundService.parkingMeterStarted = true;
                allowedParkDuration = getAllowedParkingDuration(location);
                finalAllowedParkDuration[0] = allowedParkDuration[0];
                finalAllowedParkDuration[1] = allowedParkDuration[1];
                SharedPreferencesHelper.putSharedPreferencesInt(getContext(), "AllowedParkDuration0", allowedParkDuration[0]);
                SharedPreferencesHelper.putSharedPreferencesInt(getContext(), "AllowedParkDuration1", allowedParkDuration[1]);
                Log.i("MODE", "Detection mode completed");
            }
        }
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
    }
}