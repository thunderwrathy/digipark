package com.example.digipark.ui.settings;

import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.digipark.R;
import com.example.digipark.sharedprefhelper.SharedPreferencesHelper;
import com.google.android.material.textfield.TextInputEditText;

import eltos.simpledialogfragment.SimpleDialog;
import eltos.simpledialogfragment.color.SimpleColorDialog;


public class SettingsFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_settings, container, false);

        // ----- Notifications -----
        final TextInputEditText ti_notif_count = root.findViewById(R.id.ti_notif_count);
        ti_notif_count.setHint("Input # here");

        final LinearLayout ll_v_notif = root.findViewById(R.id.ll_v_notif);
        final Button b_set_notifs = new Button(getContext());

        root.findViewById(R.id.b_notif_count).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                ll_v_notif.removeAllViews();        // Remove previously set notification settings
                int notif_count = Integer.parseInt(ti_notif_count.getText().toString());

                for (int i = 0; i < notif_count; i++) {
                    LinearLayout l1 = new LinearLayout(getContext());
                    l1.setOrientation(LinearLayout.HORIZONTAL);
                    LinearLayout.LayoutParams p = new
                            LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    p.weight = 1.0f;

                    TextView tv1 = new TextView(getContext());
                    tv1.setText("Notification "+ (i+1));
                    tv1.setTextColor(getResources().getColor(R.color.text_normal));
                    l1.addView(tv1);

                    TextInputEditText ti1 = new TextInputEditText(getContext());
                    ti1.setLayoutParams(p);
                    ti1.setHint("... min");
                    ti1.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                    ti1.setInputType(InputType.TYPE_CLASS_NUMBER);
                    ti1.setTextColor(getResources().getColor(R.color.text_normal));
                    l1.addView(ti1);

                    ll_v_notif.addView(l1);
                }
                b_set_notifs.setText(getResources().getString(R.string.notif_fin_button));
                ll_v_notif.addView(b_set_notifs);
            }
        });

        b_set_notifs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout ll;
                TextInputEditText ti;
                int notifCount = ll_v_notif.getChildCount()-1;
                SharedPreferencesHelper.putSharedPreferencesInt(getContext(), "NotifCount", notifCount);

                for (int i = 0; i < notifCount; i++) {
                    ll = (LinearLayout) ll_v_notif.getChildAt(i);
                    ti = (TextInputEditText) ll.getChildAt(1);
                    SharedPreferencesHelper.putSharedPreferencesInt(getContext(), ("Notif"+i), Integer.parseInt(ti.getText().toString()));
                }
                b_set_notifs.setText("Options saved");

            }
        });


        TextInputEditText ti_temp_thresh = root.findViewById(R.id.ti_temp_thresh);
        ti_temp_thresh.setText(""+SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "TempThresh", 40));

        TextInputEditText ti_mic_thresh = root.findViewById(R.id.ti_mic_thresh);
        ti_mic_thresh.setText(""+SharedPreferencesHelper.getSharedPreferencesInt(getContext(), "MicThresh", 40));

        final Button b_save = new Button(getContext());

        LinearLayout ll_nd = root.findViewById(R.id.ll_nd);

        b_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean tempThreshEnable = SharedPreferencesHelper.getSharedPreferencesBoolean(getContext(), "TempThreshEnable", false);

                if (tempThreshEnable) {
                    TextInputEditText ti_temp_thresh = root.findViewById(R.id.ti_temp_thresh);
                    int tempThresh = Integer.parseInt(ti_temp_thresh.getText().toString());
                    SharedPreferencesHelper.putSharedPreferencesInt(getContext(), "TempThresh", tempThresh);
                }

                boolean micThreshEnable = SharedPreferencesHelper.getSharedPreferencesBoolean(getContext(), "MicThreshEnable", false);

                if (micThreshEnable) {
                    TextInputEditText ti_mic_thresh = root.findViewById(R.id.ti_mic_thresh);
                    int micThresh = Integer.parseInt(ti_mic_thresh.getText().toString());
                    SharedPreferencesHelper.putSharedPreferencesInt(getContext(), "MicThresh", micThresh);
                }
                b_save.setText(getString(R.string.settings_b_save));
            }
        });
        b_save.setText(getString(R.string.settings_b_save_default));
        ll_nd.addView(b_save);


        // ----- Map Color -----

        final Button b_toggleMapColor = root.findViewById(R.id.b_toggleMapColor);

        b_toggleMapColor.setText(SharedPreferencesHelper.getSharedPreferencesString(getContext(),"mapColor", getString(R.string.settings_map_dark)));
        if (b_toggleMapColor.getText().equals(getString(R.string.settings_map_light))) {
            b_toggleMapColor.setBackgroundColor(getResources().getColor(R.color.mapToggleLight));
        } else {
            b_toggleMapColor.setBackgroundColor(getResources().getColor(R.color.mapToggleDark));
        }
        SharedPreferencesHelper.putSharedPreferencesString(getContext(), "mapColor",  (String)b_toggleMapColor.getText());

        b_toggleMapColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (b_toggleMapColor.getText().equals(getString(R.string.settings_map_light))) {
                    b_toggleMapColor.setText(getString(R.string.settings_map_dark));
                    b_toggleMapColor.setBackgroundColor(getResources().getColor(R.color.mapToggleDark));
                } else {
                    b_toggleMapColor.setText(getString(R.string.settings_map_light));
                    b_toggleMapColor.setBackgroundColor(getResources().getColor(R.color.mapToggleLight));
                }
                SharedPreferencesHelper.putSharedPreferencesString(getContext(), "mapColor", (String)b_toggleMapColor.getText());

            }
        });

        // Setting: Show parking zone

        RadioButton rb_1 = root.findViewById(R.id.radioButton);
        RadioButton rb_2 = root.findViewById(R.id.radioButton2);
        switch(SharedPreferencesHelper.getSharedPreferencesString(getContext(), "StpzCity", "sbg")) {
            case "sbg":
                rb_1.setChecked(true);
                break;
            case "vienna":
                rb_2.setChecked(true);
                break;
        }

        rb_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferencesHelper.putSharedPreferencesString(getContext(),"StpzCity", "sbg");
                    SharedPreferencesHelper.putSharedPreferencesString(getContext(), "StpzCityLat", "47.79941");
                    SharedPreferencesHelper.putSharedPreferencesString(getContext(), "StpzCityLon", "13.04399");
                } else {
                    SharedPreferencesHelper.putSharedPreferencesString(getContext(), "StpzCity", "vienna");
                    SharedPreferencesHelper.putSharedPreferencesString(getContext(), "StpzCityLat", "48.210033");
                    SharedPreferencesHelper.putSharedPreferencesString(getContext(), "StpzCityLon", "16.363449");
                }
            }
        });

        // Enable or disable temperature warning

        CheckBox cb1 = root.findViewById(R.id.temp_warning_enable);
        boolean tempThreshEnable = SharedPreferencesHelper.getSharedPreferencesBoolean(getContext(), "TempThreshEnable", false);
        if (tempThreshEnable) {
            cb1.setChecked(true);
        } else if (!(tempThreshEnable)) {
            cb1.setChecked(false);
        }


        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferencesHelper.putSharedPreferencesBoolean(getContext(), "TempThreshEnable", true);
                }
                else {
                    SharedPreferencesHelper.putSharedPreferencesBoolean(getContext(), "TempThreshEnable", false);
                }
            }
        });


        // Enable or disable Mic warning

        CheckBox cb2 = root.findViewById(R.id.mic_warning_enable);
        boolean micThreshEnabled = SharedPreferencesHelper.getSharedPreferencesBoolean(getContext(), "MicThreshEnable", false);
        if (micThreshEnabled) {
            cb2.setChecked(true);
        } else if (!(micThreshEnabled)) {
            cb2.setChecked(false);
        }


        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferencesHelper.putSharedPreferencesBoolean(getContext(), "MicThreshEnable", true);
                }
                else {
                    SharedPreferencesHelper.putSharedPreferencesBoolean(getContext(), "MicThreshEnable", false);
                }
            }
        });

        return root;
    }
}