package com.example.digipark.ui.parkingzones;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.DialogFragment;


public class ParkingZoneInfo extends DialogFragment {

    String msg;

    public ParkingZoneInfo(String[] message, String city) {
        if (city.equals("sbg")) {
            msg = "Name: "+message[2]+"\n"+
                    "ID: "+message[0]+"\n"+
                    "Art: "+message[1]+"\n"+
                    "Status: "+message[3]+"\n"+
                    "Maximale Parkdauer: "+message[5]+"\n"+
                    "Info: "+message[4];
        }
        else {
            msg = "Bezirk: "+message[0]+"\n";
            if (!message[1].equals("null")) {           // When stpz stretches not over 2 districts
                msg += "Bezirk2: " + message[1] + "\n";
            }
            msg +=  "Maximale Parkdauer: "+message[2]+"\n"+
                    "Info: "+message[3]+"\n"+
                    "Link: "+message[4];
        }
        Log.e("STPZ-Info", ""+msg);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Using Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }
}
