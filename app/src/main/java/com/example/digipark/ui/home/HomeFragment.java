package com.example.digipark.ui.home;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.digipark.R;
import com.example.digipark.backgroundServices.BackgroundService;


public class HomeFragment extends Fragment {

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        if (BackgroundService.parkingMeterStarted) {
            root.findViewById(R.id.button_start).setVisibility(View.GONE);
            TextView tv_info = root.findViewById(R.id.tv_info_string);
            tv_info.setText(R.string.tv_info_msg);
            Button button = new Button(getContext());
            LinearLayout linLay = root.findViewById(R.id.linearLayout);
            linLay.addView(button);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NavController navController = NavHostFragment.findNavController(HomeFragment.this);
                    navController.navigate(R.id.action_nav_home_to_homeFragment2);
                }
            });

            return root;
        }

        root.findViewById(R.id.button_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().startService(new Intent(getActivity(), BackgroundService.class));     // Starts the background service
                NavController navController = NavHostFragment.findNavController(HomeFragment.this);
                navController.navigate(R.id.action_nav_home_to_homeFragment2);
            }
        });
        return root;
    }
}