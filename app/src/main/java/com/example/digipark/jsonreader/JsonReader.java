package com.example.digipark.jsonreader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;


public class JsonReader {

    public String stringFromJsonFilestream(InputStream fileStream) throws IOException {
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        Reader reader = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
        int n;
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        fileStream.close();
        String jsonString = writer.toString();
        return jsonString;
    }

    public JSONArray getGeometryArray(String jsonString) throws JSONException {
        JSONObject obj = new JSONObject(jsonString);
        JSONArray features_arr = obj.getJSONArray("features");
        JSONArray geometryArray= new JSONArray();

        JSONObject geometryObj;

        for (int i = 0; i < features_arr.length(); i++)
        {
            geometryObj = features_arr.getJSONObject(i).getJSONObject("geometry");
            geometryArray.put(i, geometryObj);
        }
        return geometryArray;
    }

    public JSONObject getStpzProperties(String jsonString, int i) throws JSONException {       // The position which the stpz has in the features array
        JSONObject obj = new JSONObject(jsonString);
        JSONArray features_arr = obj.getJSONArray("features");
        JSONObject propertiesObj;

        propertiesObj = features_arr.getJSONObject(i).getJSONObject("properties");

        return propertiesObj;
    }

    public int[] getAllowedParkTime(JSONObject propertiesObject) throws JSONException {
        String maxParkDurationStr = propertiesObject.getString("MAXIMALE_PARKDAUER");
        int[] maxParkDuration = {0, 0};

        int parsedNumber = Integer.parseInt(maxParkDurationStr.replaceAll("\\D", ""));   // Converts the number from the string to an integer
        if (maxParkDurationStr.contains("Stunde") || maxParkDurationStr.contains("h")) {
            maxParkDuration[0] = parsedNumber;
        } else {
            maxParkDuration[0] = parsedNumber / 60;
            maxParkDuration[1] = parsedNumber - maxParkDuration[0] * 60;
        }
        return maxParkDuration;
    }

    public String[] getPropertiesInfoArr(JSONObject propertiesObject, String city) throws JSONException {
        String[] properties = new String[6];

        if (city.equals("sbg")) {
            properties[0] = propertiesObject.getString("ID");
            properties[1] = propertiesObject.getString("ART");
            properties[2] = propertiesObject.getString("NAME");
            properties[3] = propertiesObject.getString("STATUS");
            properties[4] = propertiesObject.getString("GEBUEHRENPFLICHT");
            properties[5] = propertiesObject.getString("MAXIMALE_PARKDAUER");
            return properties;
        }
        else {
            properties[0] = propertiesObject.getString("BEZIRK");
            properties[1] = propertiesObject.getString("BEZIRK2");
            properties[2] = propertiesObject.getString("DAUER");
            properties[3] = propertiesObject.getString("ZEITRAUM");
            properties[4] = propertiesObject.getString("WEBLINK1");
            return properties;
        }
    }
}