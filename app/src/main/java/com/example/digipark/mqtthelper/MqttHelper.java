package com.example.digipark.mqtthelper;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class MqttHelper {

    public MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();

    public Boolean connectedToServer = false;
    String serverUri, clientId, username, password;

    public MqttHelper(Context context, String serverUri, String clientId, String username, String password) {
        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId);

        this.serverUri = serverUri;
        this.clientId = clientId;
        this.username = username;
        this.password = password;

        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
    }

    public void setCallback(MqttCallbackExtended callback) {
        mqttAndroidClient.setCallback(callback);
    }

    public void connect() {
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    connectedToServer = true;
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt", "Failed to connect to: " + serverUri + exception.toString());
                    connectedToServer = false;
                }
            });
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    public void subscribeToTopic(String topic) {
        if (connectedToServer) {
            try {
                mqttAndroidClient.subscribe(topic, 1, null, new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Log.w("Mqtt", "Subscribed!");
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.w("Mqtt", "Subscribe failed!");
                    }
                });

            } catch (MqttException e) {
                System.err.println("Exception whilst subscribing");
                e.printStackTrace();
            }
        }
    }

    public void publishToTopic(String topic, String message) {
        if (connectedToServer) {
            MqttMessage mqttmessage = new MqttMessage();
            mqttmessage.setPayload(message.getBytes());
            try {
                mqttAndroidClient.publish(topic, mqttmessage);
            } catch (MqttException e) {
                System.err.println("Exception whilst publishing");
                e.printStackTrace();
            }
        }
    }

    public void closeConnection() {
        mqttAndroidClient.close();
    }
}




